# Proyecto

This is the Front_end part of the Cam2U web app realized as final project for the HNC in Web Development at IES Aguadulce.

## How to install

You need to have node & npm installed at your computer and the Cam2U API as well.
At the main folder execute npm install at terminal in order to get and install all the packages and dependencies.
After a few minutes (you can you and take a coffee, seriously) it will be ready to run the application (ng serve).

## Whow do I talk to?
You can contact me @ oscarmunozdev@gmail.com

